/**
 * 
 */
package com.gmail.gohnxp;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.gmail.gohnxp.threads.ConsoleListener;
import com.gmail.gohnxp.threads.JmsClient;
import com.gmail.gohnxp.threads.JmsClientWrapper;
import com.gmail.gohnxp.threads.Request1Sender;
import com.gmail.gohnxp.utils.ConfigKey;

/**
 * @author gohnxp@gmail.com
 *
 */
public class Main {

	final static Logger logger = Logger.getLogger(Main.class);

	private static boolean isNotLegal(){
//		final Calendar expirationDate = GregorianCalendar.getInstance();
//		expirationDate.set(2015, Calendar.NOVEMBER, 4);
//		final Calendar curDate = GregorianCalendar.getInstance();
		return false;//curDate.after(expirationDate);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {

		//TODO remove protection after paid
		if(isNotLegal()){
			logger.info("Your copy of program not legal. Exit.");
			return;
		}
		
		if (args.length == 0) {
			logger.error("No configuration filename in parameters! Program will exit.");
			System.exit(1);
		}

		new Main().start(args[0]);
	}

	private Properties loadConfig(String configPath){
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(configPath));
			logger.info("Config file data:");
			logger.info(props.toString());
		} catch (IOException e) {
			logger.error("Can't read configuration file!", e);
			return null;
		}
		return props;
	}
	
	private boolean checkConfigData(Properties props) {
		boolean isAllRight = true;

		if(props == null)
			return false;
		
		for (ConfigKey configKey : ConfigKey.values()) {
			if (!props.containsKey(configKey.name())) {
				logger.error("Config not contain key " + configKey.name() + ".");
				isAllRight = false;
				break;
			}
		}
		return isAllRight;
	}

	public void start(String configPath) {
		
		Properties props = loadConfig(configPath);
		if(!checkConfigData(props)){
			logger.error("Exit.");
			return;
		}
		
		JmsClient jmsClient = new JmsClient(props.getProperty(ConfigKey.JMSTopicName.name()),
				props.getProperty(ConfigKey.JMSServerURl.name()), null, props.getProperty(ConfigKey.JMSClientId.name()),
				props.getProperty(ConfigKey.JMSUserName.name()), props.getProperty(ConfigKey.JMSPassWord.name()), true,
				props.getProperty(ConfigKey.JMSFilter.name()), 
				props.getProperty(ConfigKey.FTPUserName.name()), 
				props.getProperty(ConfigKey.FTPPassWord.name()), 
				props.getProperty(ConfigKey.BaseDirName.name()));
		try {
			jmsClient.initializeConnection();
		} catch (Exception e) {
			logger.error("JMS connection was not established:");
			logger.error(e);
			logger.error("Exit.");
			return;
		}
		
		JmsClientWrapper jmsClientWrapper = new JmsClientWrapper(jmsClient);
		jmsClientWrapper.start();

		ConsoleListener consoleListener = new ConsoleListener(props, jmsClientWrapper);
		consoleListener.start();
		
		Request1Sender req1Sender = new Request1Sender(props,
				jmsClientWrapper, consoleListener);
		req1Sender.start();
		
		try {
			req1Sender.join();
		} catch (InterruptedException ie) {
			logger.error(ie);
		}		

		try {
			consoleListener.join();
		} catch (InterruptedException ie) {
			logger.error(ie);
		}
		
		try {
			jmsClientWrapper.join();
		} catch (InterruptedException ie) {
			logger.error(ie);
		}
		
		logger.info("All threads terminated.");
	}
}
