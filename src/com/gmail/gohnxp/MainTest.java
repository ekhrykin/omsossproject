package com.gmail.gohnxp;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.gmail.gohnxp.model.DeregisterLogToFile;
import com.gmail.gohnxp.model.RegisterLogToFile;
import com.gmail.gohnxp.model.ResultFilter;
import com.gmail.gohnxp.model.Security;
import com.gmail.gohnxp.utils.SoapEnvelopBuilder;
import com.gmail.gohnxp.utils.Utils;

public class MainTest {

	public static void main(String[] args) {
		testXmlParsing();
	}

	private static void testXmlParsing(){
		String xmlData = "<data><attr1>12</attr1><attr2>34</attr2></data>";
		System.out.println("attr1 == " + Utils.getXmlTagValue(xmlData, "attr1"));
		System.out.println("attr2 == " + Utils.getXmlTagValue(xmlData, "attr2"));
	}
	
	private static void testFtpFileDownload(){
		final String ftpServer = "ftp.zakupki.gov.ru";
		final String ftpUser = "free";
		final String ftpPass = "free";
		final String remoteFilePath = "/fcs_nsi/nsiPlacingWay/nsiPlacingWay_all_20151101000004_001.xml.zip";
		final String destFilePath = "C:\\temp\\ftpFile.zip";
		Utils.downloadFromFtp(ftpServer, ftpUser, ftpPass, remoteFilePath, destFilePath);
	}
	
	private static void testSoapDataBuilder() throws JAXBException{
		final List<String> filters = new ArrayList<String>();
		filters.add("attr1");
		filters.add("attr2");
		
		String result = new SoapEnvelopBuilder().
				setSecurity(new Security("user1", "pass1")).
				//setDeregisterLogToFile(new DeregisterLogToFile("clientId1")).build();
				setRegisterLogToFile(new RegisterLogToFile("className1", "dirName1", "compress1",
						"kmsClientId1", new ResultFilter(filters))).build();
		System.out.println("jaxb result = " + result);
	}
	
}
