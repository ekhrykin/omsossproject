package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="filter")
public class Between {
	private String name;
	private long first, second;

	public Between(){
	}

	public Between(String name, long first, long second) {
		super();
		this.name = name;
		this.first = first;
		this.second = second;
	}

	public String getName() {
		return name;
	}

	@XmlAttribute
	public void setName(String name) {
		this.name = name;
	}

	public long getFirst() {
		return first;
	}

	@XmlAttribute
	public void setFirst(long first) {
		this.first = first;
	}

	public long getSecond() {
		return second;
	}

	@XmlAttribute
	public void setSecond(long second) {
		this.second = second;
	}
}
