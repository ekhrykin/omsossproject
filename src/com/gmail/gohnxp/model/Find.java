package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="find", namespace="xmlapi_1.0")
@XmlType(propOrder = {"fullClassName", "filter"})
public class Find {
	private String fullClassName;
	private Filter filter;

	public Find(){
	}
	
	public Find(String fullClassName, Filter filter) {
		super();
		this.fullClassName = fullClassName;
		this.filter = filter;
	}

	public String getFullClassName() {
		return fullClassName;
	}

	@XmlElement
	public void setFullClassName(String fullClassName) {
		this.fullClassName = fullClassName;
	}

	public Filter getFilter() {
		return filter;
	}

	@XmlElement
	public void setFilter(Filter filter) {
		this.filter = filter;
	}
}
