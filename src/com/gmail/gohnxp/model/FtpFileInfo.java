package com.gmail.gohnxp.model;

public class FtpFileInfo {
	private String ip, filePath;

	public FtpFileInfo(String ip, String filePath){
		super();
		this.ip = ip;
		this.filePath = filePath;
	}
	
	public String getFilePath() {
		return filePath;
	}

	public String getIp() {
		return ip;
	}
}
