package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="filter")
public class Filter {
	private Between between;

	public Filter(){
	}
	
	public Filter(Between between) {
		super();
		this.between = between;
	}

	public Between getBetween() {
		return between;
	}

	@XmlElement
	public void setBetween(Between between) {
		this.between = between;
	}
}
