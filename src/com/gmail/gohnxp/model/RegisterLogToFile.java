package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="registerLogToFile", namespace="xmlapi_1.0")
public class RegisterLogToFile {
	private String fullClassName, dirName, compress, jmsClientId;
	private ResultFilter resultFilter;

	public RegisterLogToFile(){
	}
	
	public RegisterLogToFile(String fullClassName, String dirName, String compress, String jmsClientId,
			ResultFilter resultFilter) {
		super();
		this.fullClassName = fullClassName;
		this.dirName = dirName;
		this.compress = compress;
		this.jmsClientId = jmsClientId;
		this.resultFilter = resultFilter;
	}

	public String getFullClassName() {
		return fullClassName;
	}

	@XmlElement
	public void setFullClassName(String fullClassName) {
		this.fullClassName = fullClassName;
	}

	public String getDirName() {
		return dirName;
	}

	@XmlElement
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}

	public String getCompress() {
		return compress;
	}

	@XmlElement
	public void setCompress(String compress) {
		this.compress = compress;
	}

	public String getJmsClientId() {
		return jmsClientId;
	}

	@XmlElement
	public void setJmsClientId(String jmsClientId) {
		this.jmsClientId = jmsClientId;
	}

	public ResultFilter getResultFilter() {
		return resultFilter;
	}

	@XmlElement
	public void setResultFilter(ResultFilter resultFilter) {
		this.resultFilter = resultFilter;
	}
}
