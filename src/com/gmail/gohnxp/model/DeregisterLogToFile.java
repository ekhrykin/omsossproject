package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "deregisterLogToFile", namespace = "xmlapi_1.0")
public class DeregisterLogToFile {
	private String jmsClientId;

	public DeregisterLogToFile() {
	}

	public DeregisterLogToFile(String jmsClientId) {
		super();
		this.jmsClientId = jmsClientId;
	}

	public String getJmsClientId() {
		return jmsClientId;
	}

	@XmlElement
	public void setJmsClientId(String jmsClientId) {
		this.jmsClientId = jmsClientId;
	}

}
