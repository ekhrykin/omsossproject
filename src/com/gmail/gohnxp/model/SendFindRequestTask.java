package com.gmail.gohnxp.model;

import java.util.Date;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.gmail.gohnxp.utils.Utils;

public class SendFindRequestTask implements Runnable {

	private final Logger logger = Logger.getLogger(SendFindRequestTask.class);

	private final long originalStartTimeSeconds;
	private final Date configEndTime;
	private final int timeOffset, delayOffset;
	private final Properties reqProps, globalProps;
	private final Map<Integer, SendFindRequestTask> tasksInProgress;
	
	private final int threadTaskNumber;
	private boolean needToStop = false;
	
	public SendFindRequestTask(int threadTaskNumber, long startTimeSeconds, 
			int timeOffset, int delayOffset, Date configEndTime, Properties reqProps, 
			Properties globalProps, Map<Integer, SendFindRequestTask> tasksInProgress) {
		super();
		this.threadTaskNumber = threadTaskNumber;
		this.originalStartTimeSeconds = startTimeSeconds;
		this.configEndTime = configEndTime;
		this.timeOffset = timeOffset;
		this.delayOffset = delayOffset;
		this.reqProps = reqProps;
		this.globalProps = globalProps;
		this.tasksInProgress = tasksInProgress;
	}
	
	public void stopTask(){
		needToStop = true;
	}
	
	@Override
	public void run() {
		
		logger.info("Send request task with ID = " + threadTaskNumber + " started. "
				+ "You can stop it with command: 'STOPREQUEST " + threadTaskNumber + "'.");
		
		try {
			Thread.sleep((timeOffset + delayOffset) * 1000);
		} catch (InterruptedException e) {
			logger.error(e);
		}
		
		long startTimeSeconds = originalStartTimeSeconds;
		long endTimeSeconds = startTimeSeconds + timeOffset;
		FindMethod findMethod = Integer.parseInt(reqProps.getProperty("Method").trim()) == 1 ? 
				FindMethod.FIND : FindMethod.FIND_TO_FILE;
		
		while((endTimeSeconds * 1000 < configEndTime.getTime()) && !needToStop){							
			Utils.sendFindRequest(findMethod, reqProps, globalProps, startTimeSeconds * 1000, endTimeSeconds * 1000, 
					reqProps.getProperty("FutureFileName") + "_" + startTimeSeconds + "_" + endTimeSeconds);
			
			try {
				Thread.sleep(timeOffset * 1000);
			} catch (InterruptedException e) {
				logger.error(e);
			}

			startTimeSeconds = endTimeSeconds;
			endTimeSeconds = startTimeSeconds + timeOffset;
		}
		
		logger.info("Send request task with ID = " + threadTaskNumber + " ended.");
		tasksInProgress.remove(threadTaskNumber);
	}	
}
