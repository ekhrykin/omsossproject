package com.gmail.gohnxp.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "resultFilter")
public class ResultFilter {
	private List<String> attrs;

	public ResultFilter() {
	}

	public ResultFilter(List<String> attrs) {
		super();
		this.attrs = attrs;
	}

	@XmlElement(name = "attribute")
	public List<String> getAttrs() {
		return attrs;
	}

	public void setAttrs(List<String> attrs) {
		this.attrs = attrs;
	}
}
