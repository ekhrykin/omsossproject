package com.gmail.gohnxp.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="findToFile", namespace="xmlapi_1.0")
@XmlType(propOrder = {"fullClassName", "filter", "fileName"})
public class FindToFile {
	private String fullClassName, fileName;
	private Filter filter;

	public FindToFile(){
	}
	
	public FindToFile(String fullClassName, Filter filter, String fileName) {
		super();
		this.fullClassName = fullClassName;
		this.filter = filter;
		this.fileName = fileName;
	}

	public String getFullClassName() {
		return fullClassName;
	}

	@XmlElement
	public void setFullClassName(String fullClassName) {
		this.fullClassName = fullClassName;
	}

	public String getFileName() {
		return fileName;
	}

	@XmlElement
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Filter getFilter() {
		return filter;
	}

	@XmlElement
	public void setFilter(Filter filter) {
		this.filter = filter;
	}
}
