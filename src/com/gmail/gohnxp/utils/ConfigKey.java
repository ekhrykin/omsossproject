package com.gmail.gohnxp.utils;

public enum ConfigKey {
	ServerUrl, UserName, Password, JMSServerURl, JMSUserName, JMSPassWord,
	JMSTopicName, JMSFilter, JMSClientId, MyClasses, MyAttribute, DirName,
	BaseDirName, FTPUserName, FTPPassWord, ToCompress
}
