package com.gmail.gohnxp.utils;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.gmail.gohnxp.model.Between;
import com.gmail.gohnxp.model.Filter;
import com.gmail.gohnxp.model.Find;
import com.gmail.gohnxp.model.FindMethod;
import com.gmail.gohnxp.model.FindToFile;
import com.gmail.gohnxp.model.FtpFileInfo;
import com.gmail.gohnxp.model.Security;

public class Utils {

	private static Logger logger = Logger.getLogger(Utils.class);

	public static boolean downloadFromFtp(String ftpServer, String ftpUser, String ftpPass, 
			String remoteFilePath, String destFilePath) {

		boolean result = true;

		FTPClient ftpClient = new FTPClient();

		try {
			ftpClient.connect(ftpServer);
			ftpClient.login(ftpUser, ftpPass);
            ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			OutputStream os = new BufferedOutputStream(new FileOutputStream(destFilePath));
			result = ftpClient.retrieveFile(remoteFilePath, os);
			os.flush();
			os.close();
			logger.info("FTP file download result == " + result);
		} catch (Exception ex) {
			result = false;
			logger.error(ex);
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException iox) {
				logger.error(iox);
			}
		}

		return result;
	}

	public static boolean sendSoapPost(String soapData, String wsUrl) throws ClientProtocolException, IOException {
		boolean isPostSuccessful = true;
		HttpClient httpclient = new DefaultHttpClient();

		StringEntity strEntity = new StringEntity(soapData, "text/xml", "UTF-8");
		HttpPost post = new HttpPost(wsUrl);
		post.setHeader("SOAPAction", "CAI3G#Create");
		post.setEntity(strEntity);

		HttpResponse response = httpclient.execute(post);
		HttpEntity respEntity = response.getEntity();

		if (respEntity != null) {
			logger.info("Response for request:");
			logger.info(EntityUtils.toString(respEntity));
		} else {
			logger.error("No Response for request");
			isPostSuccessful = false;
		}
		return isPostSuccessful;
	}
	
	public static void sendFindRequest(FindMethod findMethod, Properties reqProps, Properties globalProps,
			long startTime, long endTime, String fileName){
		try {
			SoapFindEnvelopBuilder soapBuilder = new SoapFindEnvelopBuilder().
					setSecurity(new Security(globalProps.get(ConfigKey.UserName.name()).toString(), 
							globalProps.get(ConfigKey.Password.name()).toString())).
					setRequestId(globalProps.getProperty(ConfigKey.JMSClientId.name()));
			
			if(findMethod == FindMethod.FIND)			
				soapBuilder.setFind(new Find(reqProps.getProperty("ObjectName"),
								new Filter(new Between("timeCaptured", startTime, endTime))));
			else
				soapBuilder.setFindToFile(new FindToFile(reqProps.getProperty("ObjectName"),
						new Filter(new Between("timeCaptured", startTime, endTime)), fileName));
				
			String soapData = soapBuilder.build();
			
			logger.info("Find/FindToFile request == \n" + soapData);
			
			Utils.sendSoapPost(soapData, globalProps.get(ConfigKey.ServerUrl.name()).toString());
			
			logger.info("Find/FindToFile request sended");
		} catch (Exception e) {
			logger.error(e);
			logger.info("Find/FindToFile request not sended");
		}
	}

	public static String getXmlTagValue(String xmlData, String tagName){
		return xmlData.split(tagName)[1].split(">")[1].split("</")[0];
	}
	
	public static FtpFileInfo parseJmsMsg(String msgText) {
		return new FtpFileInfo(getXmlTagValue(msgText, "serverIpAddress"),
								getXmlTagValue(msgText, "fileName"));
	}
}
