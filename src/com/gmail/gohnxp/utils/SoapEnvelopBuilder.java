package com.gmail.gohnxp.utils;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.gmail.gohnxp.model.DeregisterLogToFile;
import com.gmail.gohnxp.model.RegisterLogToFile;
import com.gmail.gohnxp.model.Security;

public class SoapEnvelopBuilder {
	
	private Security security;
	private RegisterLogToFile registerLogToFile;
	private DeregisterLogToFile deregisterLogToFile;

	public SoapEnvelopBuilder setSecurity(Security security) {
		this.security = security;
		return this;
	}

	public SoapEnvelopBuilder setRegisterLogToFile(RegisterLogToFile registerLogToFile) {
		this.registerLogToFile = registerLogToFile;
		return this;
	}

	public SoapEnvelopBuilder setDeregisterLogToFile(DeregisterLogToFile deregisterLogToFile) {
		this.deregisterLogToFile = deregisterLogToFile;
		return this;
	}

	public String build() throws JAXBException {
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
							"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" + 
								"<SOAP:Header>\n" +
									"<header xmlns=\"xmlapi_1.0\">\n" +
										getSecurityStr() +
										"<requestID>" + 
										(registerLogToFile != null ? 
												registerLogToFile.getJmsClientId() : 
													deregisterLogToFile.getJmsClientId()) + "</requestID>\n" +
									"</header>\n" +
								"</SOAP:Header>\n" +
								"<SOAP:Body>" +
								(registerLogToFile != null ? getRegisterLogToFileStr() : getDeregisterLogToFileStr()) +
								"</SOAP:Body>\n" +
							"</SOAP:Envelope>";
		return result;
	}

	private String fieldToStr(Object obj, Class clazz) throws JAXBException {
		final JAXBContext jc = JAXBContext.newInstance(clazz);
        final StringWriter sw = new StringWriter();
        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        m.marshal(obj, sw);
		return sw.toString();
	}
	
	private String getDeregisterLogToFileStr() throws JAXBException {
		return fieldToStr(deregisterLogToFile, DeregisterLogToFile.class);
	}

	private String getRegisterLogToFileStr() throws JAXBException {
		return fieldToStr(registerLogToFile, RegisterLogToFile.class);
	}

	private String getSecurityStr() throws JAXBException {
		return fieldToStr(security, Security.class);
	}
}
