package com.gmail.gohnxp.utils;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.gmail.gohnxp.model.Find;
import com.gmail.gohnxp.model.FindToFile;
import com.gmail.gohnxp.model.Security;

public class SoapFindEnvelopBuilder {
	
	private Security security;
	private Find find;
	private FindToFile findToFile;
	private String requestId;

	public SoapFindEnvelopBuilder setSecurity(Security security) {
		this.security = security;
		return this;
	}

	public SoapFindEnvelopBuilder setFind(Find find) {
		this.find = find;
		return this;
	}

	public SoapFindEnvelopBuilder setFindToFile(FindToFile findToFile) {
		this.findToFile = findToFile;
		return this;
	}

	public SoapFindEnvelopBuilder setRequestId(String requestId) {
		this.requestId = requestId;
		return this;
	}

	public String build() throws JAXBException {
		String result = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
							"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" + 
								"<SOAP:Header>\n" +
									"<header xmlns=\"xmlapi_1.0\">\n" +
										getSecurityStr() +
										"<requestID>" + 
											getRequestId() +
										"</requestID>\n" +
									"</header>\n" +
								"</SOAP:Header>\n" +
								"<SOAP:Body>\n" +
									(find != null ? getFindStr() : getFindToFileStr()) +
								"</SOAP:Body>\n" +
							"</SOAP:Envelope>";
		return result;
	}

	private String fieldToStr(Object obj, Class clazz) throws JAXBException {
		final JAXBContext jc = JAXBContext.newInstance(clazz);
        final StringWriter sw = new StringWriter();
        final Marshaller m = jc.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        m.marshal(obj, sw);
		return sw.toString();
	}

	private String getFindStr() throws JAXBException {
		return fieldToStr(find, Find.class);
	}

	private String getFindToFileStr() throws JAXBException {
		return fieldToStr(findToFile, FindToFile.class);
	}

	private String getSecurityStr() throws JAXBException {
		return fieldToStr(security, Security.class);
	}

	private String getRequestId() throws JAXBException {
		return requestId;
	}
}
