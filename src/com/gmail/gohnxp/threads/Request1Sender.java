package com.gmail.gohnxp.threads;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.xml.bind.JAXBException;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.gmail.gohnxp.model.RegisterLogToFile;
import com.gmail.gohnxp.model.ResultFilter;
import com.gmail.gohnxp.model.Security;
import com.gmail.gohnxp.utils.ConfigKey;
import com.gmail.gohnxp.utils.SoapEnvelopBuilder;
import com.gmail.gohnxp.utils.Utils;

public class Request1Sender extends Thread {
	
	Logger logger = Logger.getLogger(Request1Sender.class);
	
	private final Properties props;
	private final JmsClientWrapper jmsClientWrapper;
	private final ConsoleListener consoleListener;
	
	public Request1Sender(Properties props, JmsClientWrapper jmsClientWrapper, ConsoleListener consoleListener){
		this.props = props;
		this.jmsClientWrapper = jmsClientWrapper;
		this.consoleListener = consoleListener;
	}

	@Override
	public void run() {
		super.run();
		
		boolean request1Result = true;
		try {
			request1Result = sendRequest1();
		} catch (Exception e) {
			request1Result = false;
			logger.error(e);
		}
		//TODO uncomment after debug
		if(!request1Result){
			logger.info("Response for request1 was unsuccessful. Threads will be terminated.");
			jmsClientWrapper.isNeedToExit = true;
			consoleListener.isNeedToExit = true;
		}
		
		logger.info("Request1Sender terminated.");
	}
	
	private boolean sendRequest1() throws JAXBException, ClientProtocolException, IOException{
		boolean result = false;
		
		String[] attrs = props.get(ConfigKey.MyAttribute.name()).toString().split(",");		
		final List<String> filters = Arrays.asList(attrs);
		
		String soapData = new SoapEnvelopBuilder().
				setSecurity(new Security(props.get(ConfigKey.UserName.name()).toString(), 
						props.get(ConfigKey.Password.name()).toString())).
				setRegisterLogToFile(new RegisterLogToFile(
						props.get(ConfigKey.MyClasses.name()).toString(), 
						props.get(ConfigKey.DirName.name()).toString(), 
						"false",//props.get(ConfigKey.ToCompress.name()).toString(),
						props.get(ConfigKey.JMSClientId.name()).toString(), //TODO is it right?
						new ResultFilter(filters)
						)).build();
		
		logger.info("Request1 for register file msg: " + soapData);
		result = Utils.sendSoapPost(soapData, props.get(ConfigKey.ServerUrl.name()).toString());
		logger.info("Request1 == " + result);
		
		return result;
	}
}
