package com.gmail.gohnxp.threads;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import com.gmail.gohnxp.model.Between;
import com.gmail.gohnxp.model.DeregisterLogToFile;
import com.gmail.gohnxp.model.Filter;
import com.gmail.gohnxp.model.Find;
import com.gmail.gohnxp.model.FindMethod;
import com.gmail.gohnxp.model.FindToFile;
import com.gmail.gohnxp.model.Security;
import com.gmail.gohnxp.model.SendFindRequestTask;
import com.gmail.gohnxp.utils.ConfigKey;
import com.gmail.gohnxp.utils.SoapEnvelopBuilder;
import com.gmail.gohnxp.utils.SoapFindEnvelopBuilder;
import com.gmail.gohnxp.utils.Utils;

public class ConsoleListener extends Thread {

	private Logger logger = Logger.getLogger(ConsoleListener.class);
	public volatile boolean isNeedToExit = false;

	private JmsClientWrapper jmsClientWrapper;
	private final Properties props;

	public ConsoleListener(Properties props, JmsClientWrapper jmsClientWrapper) {
		this.jmsClientWrapper = jmsClientWrapper;
		this.props = props;
	}

	private SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yy");

	private boolean isPropsContainKey(Properties props, String key) {
		if (!props.containsKey(key)) {
			logger.error("Request properties does not contain " + key);
			return false;
		}
		return true;
	}

	private boolean isPropsContainInt(Properties props, String key) {
		try {
			Integer.parseInt(props.getProperty(key));
		} catch (Exception ex) {
			logger.error("Can't parse integer property " + key);
			return false;
		}
		return true;
	}

	private boolean isRequestPropsRight(Properties reqProps) {
		try {
			dateFormat.parse(reqProps.getProperty("StartTime"));
		} catch (Exception e) {
			logger.error("Can't parse StartTime param.");
			return false;
		}
		try {
			dateFormat.parse(reqProps.getProperty("EndTime"));
		} catch (Exception e) {
			logger.error("Can't parse EndTime param.");
			return false;
		}
		if (!isPropsContainKey(reqProps, "Method") || !isPropsContainKey(reqProps, "ObjectName")
				|| !isPropsContainInt(reqProps, "TimeOffSet") || !isPropsContainInt(reqProps, "DelayOffSet")
				|| !isPropsContainKey(reqProps, "HistoryFileName") || !isPropsContainKey(reqProps, "FutureFileName"))
			return false;
		return true;
	}

	private Properties getRequestProperties(String inputLine) {
		Properties res = new Properties();

		String[] parts = inputLine.split("REQUEST");
		if (parts.length != 2) {
			logger.error("There isn't request file path in params for REQUEST command.");
			return null;
		}
		String requestFilePath = parts[1].trim();

		if (!new File(requestFilePath).exists()) {
			logger.error("The request file does not exist.");
			return null;
		}
		try {
			res.load(new FileInputStream(requestFilePath));
			if (!isRequestPropsRight(res))
				return null;
			logger.info("Request config file data:");
			logger.info(res.toString());
		} catch (IOException e) {
			logger.error("Can't read request configuration file!", e);
			return null;
		}
		return res;
	}

	private Executor executor = Executors.newCachedThreadPool();
	private int taskNumber = 0;
	final private Map<Integer, SendFindRequestTask> tasksInProgress = new HashMap<Integer, SendFindRequestTask>();

	private void doFindRequestCommand(final Properties reqProps) {
		taskNumber++;
		final Date configStartTime;
		final Date configEndTime;
		final FindMethod findMethod;
		final int timeOffset = Integer.parseInt(reqProps.getProperty("TimeOffSet"));
		final int delayOffset = Integer.parseInt(reqProps.getProperty("DelayOffSet"));
		try {
			configStartTime = dateFormat.parse(reqProps.getProperty("StartTime"));
			configEndTime = dateFormat.parse(reqProps.getProperty("EndTime"));
			findMethod = Integer.parseInt(reqProps.getProperty("Method")) == 1 ? FindMethod.FIND
					: FindMethod.FIND_TO_FILE;
		} catch (ParseException e) {
			logger.error(e);
			return;
		}
		if (configEndTime.before(new Date())) {
			Utils.sendFindRequest(findMethod, reqProps, props, configStartTime.getTime(), 
					configEndTime.getTime(), reqProps.getProperty("HistoryFileName"));
		} else {
			final long curTimeSeconds = new Date().getTime() / 1000;
			final long historyEndTimeSeconds = curTimeSeconds - delayOffset - (curTimeSeconds % timeOffset);
			// send request for history data
			Utils.sendFindRequest(findMethod, reqProps, props, configStartTime.getTime(), 
					historyEndTimeSeconds * 1000, reqProps.getProperty("HistoryFileName"));
			// send while
			SendFindRequestTask sendRequestTask = new SendFindRequestTask(taskNumber, historyEndTimeSeconds, timeOffset,
					delayOffset, configEndTime, reqProps, props, tasksInProgress);
			executor.execute(sendRequestTask);
			tasksInProgress.put(taskNumber, sendRequestTask);
		}
	}

	@Override
	public void run() {
		super.run();

		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (!isNeedToExit) {
			try {
				String line = in.readLine().trim();
				if (line.equals("CLOSE")) {
					sendRequest2();
					jmsClientWrapper.isNeedToExit = true;
					isNeedToExit = true;
				} else if (line.startsWith("REQUEST")) {
					Properties reqProps = getRequestProperties(line);
					if (reqProps != null)
						doFindRequestCommand(reqProps);
				} else if (line.startsWith("STOPREQUEST")) {
					int taskNumberForStop = Integer.parseInt(line.split("STOPREQUEST")[1].trim());
					SendFindRequestTask findRequestTask = tasksInProgress.get(taskNumberForStop);
					if (findRequestTask == null)
						logger.info("File request task with ID = " + taskNumberForStop + " not finded.");
					else
						findRequestTask.stopTask();
				}
			} catch (Exception e) {
				logger.error(e);
			}
		}

		try {
			in.close();
		} catch (IOException ioe2) {
			logger.error(ioe2);
		}
		logger.info("ConsoleListener terminated.");
	}

	private void sendRequest2() {
		try {
			String soapData = new SoapEnvelopBuilder()
					.setSecurity(new Security(props.get(ConfigKey.UserName.name()).toString(),
							props.get(ConfigKey.Password.name()).toString()))
					.setDeregisterLogToFile(new DeregisterLogToFile(props.getProperty(ConfigKey.JMSClientId.name())))
					.build();

			logger.info("Request2 == " + soapData);

			Utils.sendSoapPost(soapData, props.get(ConfigKey.ServerUrl.name()).toString());

			logger.info("Request2 sended");
		} catch (Exception e) {
			logger.error(e);
			logger.info("Request2 sended");
		}
	}
}
