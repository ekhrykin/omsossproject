package com.gmail.gohnxp.threads;

import java.io.File;
import java.util.Hashtable;

import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.jms.TopicSubscriber;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.apache.log4j.Logger;

import com.gmail.gohnxp.model.FtpFileInfo;
import com.gmail.gohnxp.utils.Utils;

public class JmsClient implements ExceptionListener, MessageListener {

	final static Logger LOGGER = Logger.getLogger(JmsClient.class);

	final String ftpUser, ftpPass, destBaseDir;

	/**
	 * Support for 5.0 external JMS context.
	 */
	private static final String JMS_CONTEXT = "external/5620SamJMSServer";

	/**
	 * The connection factory.
	 */
	protected static final String CONNECTION_FACTORY = "SAMConnectionFactory";

	/**
	 * The port seperator character for initial context construction.
	 */
	protected static final char PORT_SEP_CHAR = ':';

	/**
	 * The URL seperator character for initial context construction.
	 */
	protected static final char URL_SEP_CHAR = '/';

	/**
	 * The seperator character for initial context construction.
	 */
	private static final char MULTI_SEP_CHAR = ',';

	/**
	 * This is the topic string name.
	 */
	protected String strName;

	/**
	 * This is the URL string for a topic.
	 */
	protected String strUrl;

	/**
	 * This is the port for the topic.
	 */
	protected String port;

	/**
	 * This is the high availability URL string for a topic.
	 */
	protected String strHaUrl;

	/**
	 * This is the high availability port for the topic.
	 */
	protected String haPort;

	/**
	 * This is the client id for the topic.
	 */
	protected String clientId;

	/**
	 * This is the accepted client id for the topic. This attribute contains the
	 * client id that was successfully registered with the server.
	 */
	protected String acceptedClientId;

	/**
	 * This is the user name for the topic.
	 */
	protected String user;

	/**
	 * This is the password of the user for the topic.
	 */
	protected String password;

	/**
	 * This is the message selector for the topic.
	 */
	protected String filter;

	/**
	 * This identifies if the consumer is listening to the topic.
	 */
	protected boolean isListening;

	/**
	 * This identifies if the consumer is connected.
	 */
	protected boolean isConnected = false;

	/**
	 * This identifies if the consumer has been stopped.
	 */
	protected boolean isStopped = false;

	/**
	 * The JNDI context of the connection.
	 */
	private Context jndiContext = null;

	/**
	 * The topic connection factory.
	 */
	private TopicConnectionFactory topicConnectionFactory = null;

	/**
	 * The topic connection.
	 */
	private TopicConnection topicConnection = null;

	/**
	 * The topic session.
	 */
	private TopicSession topicSession = null;

	/**
	 * The topic.
	 */
	private Topic topic = null;

	/**
	 * The topic subscriber.
	 */
	private TopicSubscriber topicSubscriber = null;

	/**
	 * Identifies if the subscriber is durable.
	 */
	private boolean isPersistent = false;

	/**
	 * Identifies if high availability is enabled.
	 */
	private boolean isHaEnabled = false;

	/**
	 * Counter for total number of messages.
	 */
	private static int counter = 1;

	/**
	 * Constructor for creating an instance of JmsClient.
	 *
	 * @param aInTopic
	 *            The topic to connect to.
	 * @param aInUrl
	 *            The server to connect to.
	 * @param aInHaUrl
	 *            The high availability server.
	 * @param aInId
	 *            The unique id for the connection.
	 * @param aInUser
	 *            The user to connect to the server with.
	 * @param aInPassword
	 *            The password for the user.
	 * @param aInIsPersistent
	 *            If the client is durable or not.
	 * @param aInFilter
	 *            The filter to use for the subscription.
	 */
	public JmsClient(String aInTopic, String aInUrl, String aInHaUrl,
			String aInId, String aInUser, String aInPassword,
			boolean aInIsPersistent, String aInFilter, String ftpUser,
			String ftpPass, String destBaseDir) {
		String[] lUrlPort = aInUrl.split(":");
		strName = aInTopic;
		strUrl = lUrlPort[0];
		port = lUrlPort[1];
		clientId = aInId;
		user = aInUser;
		password = aInPassword;
		isPersistent = aInIsPersistent;
		filter = aInFilter;
		if (aInHaUrl != null) {
			String[] lHaUrlPort = aInHaUrl.split(":");
			isHaEnabled = true;
			haPort = lHaUrlPort[1];
			strHaUrl = lHaUrlPort[0];
		}
		this.destBaseDir = destBaseDir;
		this.ftpUser = ftpUser;
		this.ftpPass = ftpPass;

		LOGGER.info("aInUrl == " + aInUrl);
		LOGGER.info("strName == " + strName);
		LOGGER.info("strUrl == " + strUrl);
		LOGGER.info("port == " + port);
		LOGGER.info("clientId == " + clientId);
		LOGGER.info("user == " + user);
		LOGGER.info("password == " + password);
		LOGGER.info("isPersistent == " + isPersistent);
		LOGGER.info("filter == " + filter);
		LOGGER.info("isHaEnabled == " + isHaEnabled);
		LOGGER.info("haPort == " + haPort);
		LOGGER.info("strHaUrl == " + strHaUrl);
		LOGGER.info("destBaseDir == " + destBaseDir);
		LOGGER.info("ftpUser == " + ftpUser);
		LOGGER.info("ftpPass == " + ftpPass);
	}

	/**
	 * This method identifies if the JMS subscription is persistent.
	 *
	 * @return True if persistent, false if non-persistent.
	 */
	public boolean isPersistent() {
		return isPersistent;
	}

	private void workWithSoapData(String soapData){
		LOGGER.info("FileInfo msg: " + soapData);
		
		FtpFileInfo ftpFileInfo = Utils.parseJmsMsg(soapData);
		
		String destFilePath = destBaseDir + 
				new File(ftpFileInfo.getFilePath()).getName();
		
		LOGGER.info("Will download file " + ftpFileInfo.getFilePath() + 
				" from server " + ftpFileInfo.getIp() + " to " + destFilePath);
		
		Utils.downloadFromFtp(ftpFileInfo.getIp(), ftpUser, ftpPass,
				ftpFileInfo.getFilePath(), destFilePath);		
	}
	
	/**
	 * This method is called by message service for each event received.
	 *
	 * @param aInMessage
	 *            The event received
	 */
	public void onMessage(Message aInMessage) {

		try {
//			LOGGER.info("Event " + counter);

			if (aInMessage instanceof TextMessage) {
				counter++;
				
				String msgText = ((TextMessage) aInMessage).getText();

//				LOGGER.info("TextMessage: " + msgText);
				
				if(msgText.toLowerCase().contains("logfileavailableevent"))
					workWithSoapData(msgText);
				
			} else if (aInMessage instanceof ObjectMessage) {
//				LOGGER.info("aInMessage instanceof ObjectMessage");
				Object lObject = ((ObjectMessage) aInMessage).getObject();
				if (lObject != null && lObject instanceof Message) {
					onMessage((Message) lObject);
				} else {
					LOGGER.info("!(lObject != null && lObject instanceof Message)");
				}
			} else {
				LOGGER.info("Invalid Message Type.");
				String msgText = aInMessage.toString();
				LOGGER.info("aInMessage.toString() == " + msgText);
				workWithSoapData(msgText);
			}
		} catch (Throwable e) {
			LOGGER.error(e);
		}
	}

	/**
	 * This method is called to initialize the connection to the server.
	 *
	 * @throws Exception
	 *             The exception thrown if a conneciton error occurs.
	 */
	public void initializeConnection() throws Exception {
		try {
			Hashtable<String, String> env = new Hashtable<String, String>();
			env.put(Context.INITIAL_CONTEXT_FACTORY,
					"org.jnp.interfaces.NamingContextFactory");
			env.put(Context.URL_PKG_PREFIXES,
					"org.jboss.naming:org.jnp.interfaces");
			env.put("jnp.disableDiscovery", "true");
			env.put("jnp.timeout", "60000");

			// check if redundancy is enabled, if so, add the high availability
			// URL to the initial context.
			if (isHaEnabled) {
				env.put(Context.PROVIDER_URL, strUrl + PORT_SEP_CHAR + port
						+ MULTI_SEP_CHAR + strHaUrl + PORT_SEP_CHAR + haPort);
				LOGGER.info("URL for app server: " + strUrl
						+ " Redundant URL: " + strHaUrl);
			} else {
				env.put(Context.PROVIDER_URL, "jnp://" + strUrl + PORT_SEP_CHAR
						+ port + URL_SEP_CHAR);
				LOGGER.info("Standalone URL for app server: " + strUrl);
			}
			jndiContext = new InitialContext(env);

			LOGGER.info("Initializing topic (" + strName + ")...");

			// get the topic connection factory.
			//
			// If you just support SAM 5.0+, you can remove the try/catch below
			// and replace
			// it with:
			// topicConnectionFactory = getExternalFactory(jndiContext);
			//
			try {
				// For redundancy, the following connection factories should be
				// used:
				// SAM Release: 2.1 - 3.0 R2 - 5620SAMConnectionFactory
				// For standalone, the following connection factories should be
				// used:
				// SAM Release: 2.1 - 3.0 R2 - UIL2ConnectionFactory
				//
				// After Release 3.0 R3, the following connection factory is
				// used
				// in both cases: SAMConnectionFactory
				topicConnectionFactory = (TopicConnectionFactory) jndiContext
						.lookup(CONNECTION_FACTORY);
			} catch (Exception e) {
				// For SAM 5.0 support of the external JMS server.
				topicConnectionFactory = getExternalFactory(jndiContext);
			}

			// To use persistent JMS, the user must have durable subscription
			// permission (i.e. durable subscription role).
			if (user != null) {
				topicConnection = topicConnectionFactory.createTopicConnection(
						user, password);
				LOGGER.info("Connection created for user: " + user);
			} else {
				topicConnection = topicConnectionFactory
						.createTopicConnection();
				LOGGER.info("Connection created.");
			}

			// Check for persistant JMS, if so, set the unique client id.
			// IMPORTANT: Client Id must be unique! In case of connection
			// failure,
			// it identifies which messages this client missed.
			if ((isPersistent) && (null == clientId)) {
				LOGGER.info("Client ID cannot be null for a durable subscription.");
				throw new JMSException(
						"Client ID cannot be null for a durable subscription.");
			}
			if ((null != clientId) && (!"".equals(clientId))) {
				topicConnection.setClientID(clientId);
				LOGGER.info("Using client id: " + clientId);
			}

			// create the topic session.
			topicSession = topicConnection.createTopicSession(false,
					TopicSession.AUTO_ACKNOWLEDGE);
			LOGGER.info("Topic session created.");

			// find the topic.
			try {
				topic = (Topic) jndiContext.lookup(strName);
			} catch (NamingException ne) {
				// For SAM 5.0 support of the external JMS server.
				Context lInitialContext = (Context) jndiContext
						.lookup(JMS_CONTEXT);
				topic = (Topic) lInitialContext.lookup(strName);
			}
			LOGGER.info("Finished initializing topic...");

			// create topic subscriber based on persistance.
			if (isPersistent) {
				// This is where the subscriber is created with durable
				// subscription
				// for persistant JMS. The client must specify a name that
				// uniquely
				// identifies each durable subscription it creates.
				if (null != filter) {
					topicSubscriber = topicSession.createDurableSubscriber(
							topic, clientId, filter, false);
					LOGGER.info("Durable topic subscriber created with filter: "
							+ filter);
				} else {
					topicSubscriber = topicSession.createDurableSubscriber(
							topic, clientId);
					LOGGER.info("Durable topic subscriber created.");
				}
			} else {
				if (null != filter) {
					topicSubscriber = topicSession.createSubscriber(topic,
							filter, false);
					LOGGER.info("Topic subscriber created with filter: "
							+ filter);
				} else {
					topicSubscriber = topicSession.createSubscriber(topic);
					LOGGER.info("Topic subscriber created.");
				}
			}
			acceptedClientId = topicConnection.getClientID();
			LOGGER.info("Client id: " + topicConnection.getClientID());
			setMessageListener(this);
			setExceptionListener(this);
			startListening();
			isConnected = true;
			LOGGER.info("Connected and listening...");
		} catch (Throwable jmse) {
			if (topicSession != null) {
				topicSession.close();
			}
			if (topicConnection != null) {
				topicConnection.close();
			}
			LOGGER.info("Exception: " + jmse.getMessage());
			isConnected = false;
			throw new JMSException(jmse.getMessage());
		}
	}

	/**
	 * This method is used for the seperate SAM JMS server (SAM 5.0+).
	 *
	 * @param aInContext
	 *            The initial context for the SAM server.
	 *
	 * @return The found connection factory.
	 *
	 * @throws NamingException
	 *             If the factory could not be found.
	 */
	private TopicConnectionFactory getExternalFactory(Context aInContext)
			throws NamingException {
		try {
			Context lInitialContext = (Context) aInContext.lookup(JMS_CONTEXT);
			return (TopicConnectionFactory) lInitialContext
					.lookup(CONNECTION_FACTORY);
		} catch (NamingException e) {
			LOGGER.info("JNDI API lookup failed: " + e.toString());
			throw e;
		}
	}

	/**
	 * This method is called when the consumer wants to start receiving
	 * messages.
	 *
	 * @throws JMSException
	 *             If an exception happens on the connection.
	 */
	public void startListening() throws JMSException {
		topicConnection.start();
		LOGGER.info("Topic subscriber Listening...");
		isListening = true;
	}

	/**
	 * This method is called when the consumer wants to stop receiving messages.
	 *
	 * @throws JMSException
	 *             If an exception happens on the connection.
	 */
	public void stopListening() throws JMSException {
		if (null != topicConnection) {
			topicConnection.stop();
		}
		LOGGER.info("Topic subscriber not Listening...");
		isListening = false;
	}

	/**
	 * This method is called to unsubscribe from a durable subscription. This
	 * MUST be called to remove the subscription from the server otherwise
	 * messages will be queued forever for this subscription.
	 *
	 * @throws JMSException
	 *             If an error occurrs unsubscribing.
	 */
	public void unsubscribe() throws JMSException {
		topicSession.unsubscribe(acceptedClientId);
	}

	/**
	 * This method sets the exception listener on the connection.
	 *
	 * @param aInListener
	 *            The connection listener to set.
	 *
	 * @throws JMSException
	 *             If an exception happens on the connection.
	 */
	public void setExceptionListener(ExceptionListener aInListener)
			throws JMSException {
		if (null != topicConnection) {
			topicConnection.setExceptionListener(aInListener);
		}
	}

	/**
	 * This method sets the message listener for the connection.
	 *
	 * @param aInListener
	 *            The message listener to receive messages.
	 */
	public void setMessageListener(MessageListener aInListener) {
		try {
			topicSubscriber.setMessageListener(aInListener);
		} catch (Exception e) {
			LOGGER.info("Exception setting message listener: "
					+ e.getMessage());
		}
	}

	/**
	 * This method is called when the consumer wants to close the connection.
	 */
	public synchronized void closeConnection() {
		close();
		isStopped = true;
	}

	/**
	 * This method is called when the consumer wants to close the connection.
	 */
	private synchronized void close() {
		try {
			isConnected = false;
			stopListening();
			try {
				topicSubscriber.close();
			} catch (Exception e) {
				LOGGER.info("Exception on subscriber close: " + e.getMessage());
			}
			if (isPersistent()) {
				try {
					unsubscribe();
				} catch (Exception e) {
					LOGGER.info("Exception on unsubscribe: " + e.getMessage());
				}
			}
			try {
				topicSession.close();
			} catch (Exception e) {
				LOGGER.info("Exception on session close: " + e.getMessage());
			}
			try {
				topicConnection.close();
			} catch (Exception e) {
				LOGGER.info("Exception on topic close: " + e.getMessage());
			}
			LOGGER.info("Topic subscriber connection closed.");
		} catch (Exception e) {
			LOGGER.info("Exception on close: " + e.getMessage());
		}
	}

	/**
	 * This method is called when an exception occurrs on the JMS connection.
	 *
	 * @param aInException
	 *            The exception that occurred.
	 */
	public void onException(JMSException aInException) {
		LOGGER.info("An Exception has occurred for the connection: "
				+ aInException.getMessage());
		try {
			setExceptionListener(null);
			topicConnection.close();
		} catch (Exception e) {
			// Ignore this exception, the TCP connection may already be closed.
		}
		if (isHaEnabled) {
			int lAttempts = 0;
			while (!isConnected && !isStopped) {
				lAttempts++;
				try {
					initializeConnection();
					return;
				} catch (Exception e) {
					LOGGER.info("Connection Attempt #: " + lAttempts
							+ " Exception: " + e.getMessage());
				}
				try {
					Thread.sleep(5000);
				} catch (Exception e) {
					// This exception should not happen unless the process
					// is killed at this point, in which case it is ignored.
				}
			}
		} else {
			LOGGER.info("Exiting...");
			System.exit(3);
		}
	}
}