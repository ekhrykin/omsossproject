package com.gmail.gohnxp.threads;

import org.apache.log4j.Logger;

public class JmsClientWrapper extends Thread {

	volatile boolean isNeedToExit = false;
	private JmsClient jmsClient;

	final Logger logger = Logger.getLogger(JmsClientWrapper.class);

	public JmsClientWrapper(JmsClient jmsClient) {
		this.jmsClient = jmsClient;
	}

	@Override
	public void run() {
		super.run();
		
		try {

			jmsClient.startListening();

			while (!isNeedToExit) {
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			logger.error(e);
		} finally {
			jmsClient.closeConnection();
		}
		logger.debug("JsmClientWrapper terminated.");
	}
}
